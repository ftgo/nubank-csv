
var pob_eng = {
  "Fev":"Feb",
  "Abr":"Apr",
  "Mai":"May",
  "Ago":"Aug",
  "Set":"Sep",
  "Out":"Oct",
  "Dez":"Dec"
}

function replaceAll(str,mapObj){
    var re = new RegExp(Object.keys(mapObj).join("|"),"g");

    return str.replace(re, function(matched){
        return mapObj[matched];
    });
}

function parseDate(str, year) {
  str = replaceAll(str, pob_eng);

  var d = new Date(str + ' ' + year);
  var day = d.getDate();
  var month = d.getMonth();
  
  var dNow = new Date();

  var year = (dNow.getFullYear() < d.getFullYear() && (month === 11 || month === 10)) || month > dNow.getMonth() ? d.getFullYear() - 1 : d.getFullYear();
  
  date = ('0' + day).slice(-2) + "/" + ('0' + (month + 1)).slice(-2) + "/" + year;
  
  // console.log(date)

  return date;
}

function getYearFromTab(str) {
  var patt = /\w{3}\s(\d{2})?/g;
  var res = patt.exec(str);
  
  // console.log(res);

  if (!res[1]) {
    return new Date().getFullYear().toString().slice(-2);
  }

  return res[1];
}

function chargesToString(charges) {
  var text = "";
  for (var charge of charges) {
    // if (charge.description !== 'Pagamento recebido') {
      var amount = charge.amount.charAt(0) == '-' ? charge.amount.replace('-','') : '-' + charge.amount;
      text += charge.date + "; " + charge.description + "; " + amount + "\n";

    // }
  }

  return text;
}

// Inform the background page that
// this tab should have a page-action
chrome.runtime.sendMessage({
  from:    'content',
  subject: 'showPageAction'
});

// Listen for messages from the popup
chrome.runtime.onMessage.addListener(function (msg, sender, response) {
  // First, validate the message's structure
  if ((msg.from === 'popup') && (msg.subject === 'DOMInfo')) {
    // Collect the necessary data
    // (For your specific requirements `document.querySelectorAll(...)`
    //  should be equivalent to jquery's `$(...)`)

    // total:   document.querySelectorAll('*').length,
    // inputs:  document.querySelectorAll('input').length,
    // buttons: document.querySelectorAll('button').length


    var msgs = new Array();
    msgs.push('Hello');
    msgs.push('Bye');

    var tabs_ = document.querySelectorAll('md-tab.active');
    if (tabs_.length) {
      var tabText = tabs_[0].innerText.trim();
    }

    var year = getYearFromTab(tabText);

    var charges = new Array();

    var charges_ = document.querySelectorAll('div.md-tab-content.ng-scope:not(.ng-hide) > div.row > div.charges > div.charges-list > div.charge');
    for (var charge_ of charges_) {
      var charge = {
        date:         parseDate(charge_.querySelectorAll(':scope > div.time')[0].textContent, year),
        description:  charge_.querySelectorAll(':scope > div.charge-data > div.description')[0].textContent.trim(),
        amount:       charge_.querySelectorAll(':scope > div.charge-data > div.amount')[0].textContent
      }

      charges.push(charge);
    }

    var info = chargesToString(charges);

    // Directly respond to the sender (popup),
    // through the specified callback */
    response(info);
  }
});
